# GitLab Environment Object Model

[[_TOC_]]

## Overview

At GitLab, we have multiple initiatives that are centered around, or significantly invested in the definition, creation, management, and cleanup of GitLab "environments", where an environment represents all of the components required for a GitLab application instance (endpoint), from the foundational cloud platform and infrastructure resources (VMs, cloud services, k8s cluster, etc.), to the deployment of GitLab and ongoing management of operating systems, application configuration, and in some cases, integration with external services/APIs.

While working with one of these projects that is subject to much of the recent focus ([GitLab Environment Toolkit][GET]), I began toying with the idea of [building a command-line interface](https://gitlab.com/craig/gitlab-environment-toolkit/-/blob/30297a4bb1155bcf287ca08763c9ec3ca8e380e4/bin/README.md) to template some of the configuration files associated with particular reference architectures, and then provide some standard ways to operate on target environments. This, in-turn, prompted the consideration of the target environment as an object, with related sub-components, the corresponding attributes associated with each resource, and inter-relationships between resources in the form of requirements or contra-indicated conflicting configurations.

We don't currently have any unified model for representing a GitLab environment, optional vs required attributes for various portions within an environment, corequisite/prerequisite configuration values, and supported operations or mutations for gracefully handling configuration changes or deployments. This is obviously not preventing development and progress of the various related initiatives, but it _may_ contribute to inefficiencies, unexpected error states, inconsistent implementations, and duplicated effort.

> Design models can be considered as a transformation of system requirements to a specification of how to implement the system. Design models have to be abstract enough to hide the unnecessary details. However, they also have to include enough detail for programmers to make implementation decisions. [[2][moodle]]

While we are not starting from formal requirements, per se, we do have examples of [existing environments built over time](gitlab-com/gitlab-com-infrastructure), [multiple examples](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/sandbox/nail) of [environments built using GET](gitlab-com/gl-infra/ansible-workloads/db-provisioning) (those are just a couple from the infrastructure team; there are many others across GitLab), and [application documentation describing the reference architectures](https://docs.gitlab.com/ee/administration/reference_architectures/). This exercise will synthesize data from multiple sources to produce the resultant schema.
Alongside this document, this proposal should include illustrative examples such as a [basic JSON schema][json-schema], [OpenAPI specification][swagger], and a Ruby class or module (and optionally a CLI wrapper), as well.

### Examples

1. [Schema](https://gitlab.com/craig/geom/-/blob/master/schema/environment)
1. [Class/Library](https://gitlab.com/craig/geom/-/blob/master/gitlab-environment.rb)
1. [API Specification](https://gitlab.com/craig/geom/-/blob/master/gitlab-environment-api.json)
1. [CLI](https://gitlab.com/craig/geom/-/blob/master/getctl.rb)

## Schema

The example schema only just represents a possible top-level interface, with some examples of sub-schema references, and distinguishing between required and optional attributes. Not yet explored, but likely to be the next most valuable aspect of following down this path, is the addition of more resources for sub-components of the system, such as the database server/cluster, redis, gitaly/praefect, etc, and establishing [conditional relationships](https://json-schema.org/understanding-json-schema/reference/conditionals.html) between them.

I've notionally included an optional top-level attribute for GitLab reference architectures, which could be extended to proscriptively enforce rather strict adherence to the reference architectures as documented, while still allowing for customizability and use for custom infrastructure deployments if needed to meet user requirements. The potential challenge there is whether the use of required attributes and conditional composition, would be sufficient to prevent breaking edge/corner cases when leveraging tools validated against this schema to deploy custom environments.

## Implementations

Providing the most potential real-world value from this exercise, is the translation of a schema into useful tools and interfaces. While none of these are necessarily required for development of quality tools or user-friendly interfaces, the point is rather in the mindset and methodology: consistent and coherent reasoning about common definitions for the resources, attributes, relations, operations, mutations, and lifecycles.

### Library/SDK

While not strictly required to exactly match the interfaces built around the specification, a shared library would be useful in multiple contexts, and provide consistency and reusability when developing APIs/CLIs/services.

For our near-term use-cases, this would serve primarily as an abstraction layer for independence from underlying implementations, so that we can:

1. Support multiple similar implementations, in cases where one may be preferred within limited contexts
1. Facilitate the migration between provisioning/deployment/configuration tools over time as requirements or the associated ecosystems change
1. Allow for custom-built solutions for functionality that is not provided by third-party tools
1. Allow for more flexible orchestration of workflows across multiple third-party tools, where integration is otherwise unavailable or prohibitively limited

### API

### CLI

Thinking primarily of the use-cases around the operational workflows for [GET][GET], this example would facilitate the generation of a new environment configuration from template files, updating existing environment configuration, and lifecycle management. Today, the toolkit aspect of GET is missing, as there are no higher-level features beyond the third-party tools and documentation comprised of lengthy, time-consuming checklists. Even if this never develops into a single tool with a uniform command-line interface, having _some_ facility to automate things like initializing a new environment configuration from a template with proper input values would be a significant reduction to the barrier to entry for deploying with GET. Similarly, the more tooling can simplify aspects of managing multiple environments (like GitLab Dedicated, or gitlab.com), or managing more complex environments as would be the most likely potential use-case for end-users, the more valuable GET becomes overall.

While adding basic automation to GET would not require investment in a fully-developed schema for this object model, subsequent iterative improvements such as adding the ability to validate an environment configuration _would_ benefit from, if not require a more comprehensive understanding of the attributes and inter-relationships of various components, as well as provider-specific features or limitations.

## External References

1. Initial draft/concept for this issue: [google doc (GitLab internal only)](https://docs.google.com/document/d/1Jow6vTeUpZphS1qpIQBaKJb7isg5XX-yeeffevmPCFw/edit#heading=h.tp98vftj4nra)
1. Software Development Processes and Software Quality Assurance: [[moodle.autolab.uni-pannon.hu][moodle]]
1. JSON Schema: [[json-schema.org][json-schema]]
1. OpenAPI Specification: [[swagger.io/specification][swagger]]
1. Understanding JSON Schema: [[json-schema.org/understanding-json-schema/][understanding-json-schema]]

[GET]: https://gitlab.com/gitlab-org/quality/gitlab-environment-toolkit
[json-schema]: https://json-schema.org
[moodle]: http://moodle.autolab.uni-pannon.hu/Mecha_tananyag/szoftverfejlesztesi_folyamatok_angol/ch05.html#d0e1940
[swagger]: https://swagger.io/specification/
[understanding-json-schema]: https://json-schema.org/understanding-json-schema/index.html
